package main

import (
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"website/handler"
)

func main() {
	router := httprouter.New()
	router.GET("/", handler.RootGet())
	router.GET("/home", handler.HomeGet())

	log.Fatal(http.ListenAndServe(":8080", router))
}
