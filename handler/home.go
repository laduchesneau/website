package handler

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func HomeGet() httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		p := "." + r.URL.Path
		if p == "./home" {
			p = "static/index.html"
		}
		http.ServeFile(w, r, p)
	}
}
