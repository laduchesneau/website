package handler

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func RootGet() httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		w.WriteHeader(http.StatusOK)
		if r.URL.Path == "" {
			_, _ = fmt.Fprintf(w, "Hello, welcome to my homepage")
		} else {
			_, _ = fmt.Fprintf(w, "Hello, you URL path is %s!", r.URL.Path[1:])
		}
	}
}
