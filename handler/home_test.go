package handler

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestHome(t *testing.T) {
	os.Chdir("..")
	tests := []struct {
		name string
		want int
	}{
		{"1", http.StatusOK},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/home", nil)
			request.Header.Set("user-Agent", "gotest")
			w := httptest.NewRecorder()

			path := HomeGet()
			path(w, request, nil)

			if w.Code != tt.want {
				t.Errorf("RootGet = %v, want %v", w.Code, tt.want)
			}
		})
	}
}
