package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoot(t *testing.T) {
	tests := []struct {
		name   string
		want   string
		status int
	}{
		{"1", "Hello, welcome to my homepage", http.StatusOK},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := httptest.NewRequest(http.MethodGet, "http://localhost:8080", nil)
			request.Header.Set("user-Agent", "gotest")
			w := httptest.NewRecorder()

			path := RootGet()
			path(w, request, nil)
			body := w.Body.String()
			if body != tt.want {
				t.Errorf("RootGet = %q, want %q", body, tt.want)
			}
			if w.Code != tt.status {
				t.Errorf("RootGet = %q, want %q", w.Code, tt.status)
			}
		})
	}
}
