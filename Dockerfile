FROM alpine


WORKDIR /app

RUN apk add --no-cache tzdata
RUN mkdir /app/static
COPY static /app/static/
COPY http-server /app/

ENTRYPOINT ["/bin/sh"]
